import UIKit
import FirebaseAuth
import FirebaseDatabase

class CreateAccountViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var retypePasswordTextField: UITextField!
    @IBOutlet weak var createAccountButton: UIButton!
    var usersRef = FIRDatabase.database().reference().child("users")
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    
    // function to display error message
    func displayVC(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
    }

    func configureView() {
        self.createAccountButton.layer.cornerRadius = 20
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        self.retypePasswordTextField.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    @IBAction func backButtonWasPressed(sender: UIButton) {
        performSegueWithIdentifier("backToHomeSegue", sender: self)
    }
    
    
    func logUserInAfterCreatingAccount(email: String, password: String) {
        
        FIRAuth.auth()?.signInWithEmail(email, password: password) {(user, error) in
            if let error = error {
                self.displayVC("There was an error signing in. Please try again")
                print(error.localizedDescription)
                return
            } else {
                self.performSegueWithIdentifier("accountCreatedSuccessfullySegue", sender: self)
                print("Account was created with success")
            }
        }
    }

    @IBAction func createAccountButtonWasPressed(sender: AnyObject) {
        let email = emailTextField.text!
        let password = passwordTextField.text!
        let password2 = retypePasswordTextField.text!
        
        // checl and see if text fields are empty
        if email != "" && password != "" && password2 != "" {
            
            if email.rangeOfString("@") != nil {
                
                if password == password2 {
                    //create user account and go back to home screen for user to login
                    FIRAuth.auth()?.createUserWithEmail(email, password: password) { (user, error) in
                        if let error = error {
                            self.displayVC("Email has already been used. Please try again with a different one")
                            print(error.localizedDescription)
                            self.emailTextField.text! = ""
                            return
                        }
                        else {
                            
                            // create a new user in the firebase database under the child users and push up email as well
                            
                            // Also create another branch in the database to store icon badge numbers
                            self.usersRef.child(user!.uid).setValue(user!.uid)
                    
                            self.logUserInAfterCreatingAccount(email, password: password)
                        }
                    }
                }
                else {
                    self.displayVC("Passwords do not match")
                }
            }
            else {
                self.displayVC("Please enter a valid email")
            }
        }
        else {
            self.displayVC("Please complete all text fields")
        }
    }
    
    
    // set the text field delegates to themselves in viewdidload in order for the following code to work.
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    // Lifting the view up
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
}























