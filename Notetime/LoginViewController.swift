import UIKit
import FirebaseAuth
import FirebaseDatabase

class LoginViewController: UIViewController, UITextFieldDelegate {
   
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var logo: UIImageView!
    
    // used for displaying error messages
    func displayVC(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func configureView() {
        self.loginButton.layer.cornerRadius = 20
        self.createAccountButton.layer.cornerRadius = 20
        
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        
        self.logo.transform = CGAffineTransformMakeScale(0.0, 0.0)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureView()
    }
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    // Lifting the view up
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    
    
    func animateLogo() {
        UIView.animateWithDuration(1.5, delay: 0.0, options: [], animations: {
            self.logo.transform = CGAffineTransformIdentity
            }, completion: nil)
    }
    
    // use this to keep user logged in
    override func viewDidAppear(animated: Bool) {
        if let _ = FIRAuth.auth()?.currentUser?.uid {
            self.performSegueWithIdentifier("segueToNotesFromEmailLogin", sender: self)
        } else {
            self.animateLogo()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // email login with firebase
    @IBAction func loginButtonWasPressed(sender: AnyObject) {
        let email = emailTextField.text!
        let password = passwordTextField.text!
        
        // check if the text fields are empty 
        if email != "" && password != "" {
        
            FIRAuth.auth()?.signInWithEmail(email, password: password) {(user, error) in
                if let error = error {
                    self.displayVC("Email or password is incorrect")
                    print(error.localizedDescription)
                    return
                } else {
                    self.performSegueWithIdentifier("segueToNotesFromEmailLogin", sender: self)
                }
            }
        }
        else {
            self.displayVC("Please enter Email and Password")
        }
    }
}
