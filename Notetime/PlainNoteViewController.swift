import UIKit
import FirebaseAuth
import FirebaseDatabase

class PlainNoteViewController: UIViewController {
    
    @IBOutlet weak var plainNoteTitleTextField: UITextField!
    @IBOutlet weak var bodyOfNoteTextView: UITextView!
    @IBOutlet weak var doneButton: UIButton!
    let usersRef = FIRDatabase.database().reference().child("users")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.doneButton.layer.cornerRadius = 15
        self.bodyOfNoteTextView.layer.cornerRadius = 15
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /* a function that gets the title and all the text and stores it in firebase database in plain note childs
       under users child */
    func grabDataAndPushToFirebase() {
        let title = plainNoteTitleTextField.text!
        let note = bodyOfNoteTextView.text!
        let usersId = FIRAuth.auth()?.currentUser?.uid
        
        if title != "" || note != "" {
            let data = [
                "title": "📝 " + title,
                "note": note,
                "noteType": "PlainNote"
            ]
            
        self.usersRef.child(usersId!).childByAutoId().setValue(data)
        }
    }
    
    // MARK: - Navigation
    @IBAction func navButtonWasPressed(sender: UIButton) {
        grabDataAndPushToFirebase()
        
        let button = sender.currentTitle!
        
        switch button {
        case "📖 All Notes":
            self.performSegueWithIdentifier("plainNoteToAllNotesSegue", sender: self)
        case "🕔 Timed Task":
            self.performSegueWithIdentifier("plainNoteToTimedTaskSegue", sender: self)
        case "Save":
            self.performSegueWithIdentifier("plainNoteToAllNotesSegue", sender: self)
        default: break
        }
    }
}















