import UIKit
import FirebaseDatabase
import FirebaseAuth

extension String {
    
    subscript (i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = startIndex.advancedBy(r.startIndex)
        let end = start.advancedBy(r.endIndex - r.startIndex)
        return self[Range(start ..< end)]
    }
}

// THIS DOESNT WORK WHEN THERE IS A TABLEVIEW INVOLVED
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}


class AllNotesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    var notes = [String]()
    
    var noteBody = [String]()
    var uniqueReferences = [String]()
    var specificRef: String?
    var snapShots = [FIRDataSnapshot]()
    var specificSnapshot: FIRDataSnapshot?
    let usersRef = FIRDatabase.database().reference().child("users")
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchBarAboveNoteTypes: UISearchBar!
    
    var filteredNotes = [String]()
    var filteredNoteBodies = [String]()
    var shouldShowSearchResults = false
    
    var noteAndBodyForSearch = [(note: String, body: String, specificRef: String, specificSnapShot: FIRDataSnapshot)]()
    
    var filteredNoteAndBodyForSearch = [(note: String, body: String, specificRef: String, specificSnapShot: FIRDataSnapshot)]()
    
    var isFirstTime = true
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchBarAboveNoteTypes.delegate = self
        self.searchBarAboveNoteTypes.showsCancelButton = true
        
        let textFieldInsideSearchBar = self.searchBarAboveNoteTypes.valueForKey("searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.whiteColor()
        
        let notificationCount = UIApplication.sharedApplication().scheduledLocalNotifications?.count
        print("Number of notifications sceduled from the all notes view controller: \(notificationCount)")
        
        // UIApplication.sharedApplication().cancelAllLocalNotifications()
    }
    
    // set up arrays for viewing in the tableview
    override func viewDidAppear(animated: Bool) {
        var localNotes = [String]()
        var localBody = [String]()
        var localReferences = [String]()
        var localSnapShot = [FIRDataSnapshot]()
        let usersId = FIRAuth.auth()?.currentUser?.uid
        var first50CharsOfPlainNote = ""
        
        self.usersRef.child(usersId!).observeEventType(.ChildAdded, withBlock: { snapshot in
            
            let title = snapshot.value?.objectForKey("title") as! String
            let noteType = snapshot.value?.objectForKey("noteType") as! String
            let uniqueReference = snapshot.key
            
            if noteType == "PlainNote" {
                first50CharsOfPlainNote = snapshot.value?.objectForKey("note") as! String
                
                if first50CharsOfPlainNote.characters.count > 21 {
                    first50CharsOfPlainNote = first50CharsOfPlainNote[0...21] + "..."
                } else {
                    first50CharsOfPlainNote = first50CharsOfPlainNote + "..."
                }
                
                localBody.insert(first50CharsOfPlainNote, atIndex: 0)
                
            } else if noteType == "TimedTask" {
                localBody.insert("Timed Task", atIndex: 0)
            }
            
            localNotes.insert(title, atIndex: 0)
            localReferences.insert(uniqueReference, atIndex: 0)
            localSnapShot.insert(snapshot, atIndex: 0)
            
            self.notes = localNotes
            self.noteBody = localBody
            self.uniqueReferences = localReferences
            self.snapShots = localSnapShot
            
            self.tableView.reloadData()
        })
    }
    
    
    // MARK: - Setting up search bar
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        self.shouldShowSearchResults = true
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        self.shouldShowSearchResults = false
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.shouldShowSearchResults = false
        self.searchBarAboveNoteTypes.text = ""
        view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.shouldShowSearchResults = false
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if self.notes.count > 1 {
        
            if self.isFirstTime == true {
                for i in 0...self.notes.count - 1 {
                    self.noteAndBodyForSearch += [(note: self.notes[i], body: self.noteBody[i], specificRef: self.uniqueReferences[i], specificSnapshot: self.snapShots[i])]
                }
            
                self.isFirstTime = false
            }
        }
        
        // takes care of the filtering
        self.filteredNoteAndBodyForSearch = self.noteAndBodyForSearch.filter({ (text) -> Bool in
            let tmp: NSString = text.note
            let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })

        // should show the results condition
        if self.filteredNoteAndBodyForSearch.count == 0 {
            self.shouldShowSearchResults = false
        } else {
            self.shouldShowSearchResults = true
        }
        
        self.tableView.reloadData()
    }
    
    
    
    
    // MARK: - Setting up TableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.shouldShowSearchResults == true {
            return filteredNoteAndBodyForSearch.count
        } else {
            return notes.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        
        // Show the correct filter.
        if self.shouldShowSearchResults == true {
            cell.textLabel?.text = self.filteredNoteAndBodyForSearch[indexPath.row].note
            cell.detailTextLabel?.text = self.filteredNoteAndBodyForSearch[indexPath.row].body
        } else {
            cell.textLabel?.text = notes[indexPath.row]
            cell.detailTextLabel?.text = noteBody[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if self.shouldShowSearchResults == true {
            return false
        } else {
            return true
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == .Delete {
            
            let usersId = FIRAuth.auth()?.currentUser?.uid
            var reference: String?
            
            // deleting the wrong indexPath.row!
            
            // need to delete the correct one here
            reference = self.uniqueReferences[indexPath.row]
            
            let snapshot = snapShots[indexPath.row]
            let noteType = snapshot.value?.objectForKey("noteType") as! String
            
            if noteType == "TimedTask" {
                let uniqueIDs = snapshot.value?.objectForKey("UniqueIDsForNotifications") as! NSArray as! [String]
                
                let num = uniqueIDs.count
                
                // delete notifications!
                for i in 0..<num {
                
                    let storedID = uniqueIDs[i]
                    
                    let scheduledNotificationCount = UIApplication.sharedApplication().scheduledLocalNotifications!.count
                    
                    if scheduledNotificationCount != 0 {
                        for x in 0..<scheduledNotificationCount {
                        
                            let notification = UIApplication.sharedApplication().scheduledLocalNotifications![x]
                        
                            let notificationUid = String(notification.userInfo!["uid"]!)
                        
                            if storedID == notificationUid {
                                UIApplication.sharedApplication().cancelLocalNotification(notification)
                                break
                            }
                        }
                    }
                }
            }
            
            // go through each childByAutoId and see which one to delete
            usersRef.child(usersId!).observeEventType(.ChildAdded, withBlock: { snapshot in
                let uniqueId = snapshot.key
                if reference == uniqueId {
                    self.usersRef.child(usersId!).child(snapshot.key).removeValue()
                }
            })
            
            if self.noteAndBodyForSearch.count > 0 {
                self.noteAndBodyForSearch.removeAtIndex(indexPath.row)
            }
            
            self.notes.removeAtIndex(indexPath.row)
            self.noteBody.removeAtIndex(indexPath.row)
            self.uniqueReferences.removeAtIndex(indexPath.row)
            self.snapShots.removeAtIndex(indexPath.row)
            
            self.tableView.reloadData()
        }
    }
    
    // tableview cell was tapped
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        
        
        // need to check whether it is filtered or not and get the right reference.
        if self.shouldShowSearchResults == true {
            self.specificRef = self.filteredNoteAndBodyForSearch[indexPath.row].specificRef
            self.specificSnapshot = self.filteredNoteAndBodyForSearch[indexPath.row].specificSnapShot
        } else {
            self.specificRef = self.uniqueReferences[indexPath.row]
            self.specificSnapshot = self.snapShots[indexPath.row]
        }
        
        let noteType = specificSnapshot!.value?.objectForKey("noteType") as! String
        
        if noteType == "PlainNote" {
            self.performSegueWithIdentifier("allNotesToEditPlainNoteSegue", sender: self)
        } else if noteType == "TimedTask" {
            self.performSegueWithIdentifier("allNotesToEditTask2Segue", sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
    }
    
    // MARK: - Navigation
    @IBAction func navButtonWasPressed(sender: UIButton) {
        let button = sender.currentTitle!
        
        switch button {
        case "🕔 Timed Task":
            self.performSegueWithIdentifier("allNotesToTimedTaskSegue", sender: self)
        case "📝 Plain Note":
            self.performSegueWithIdentifier("allNotesToPlainNoteSegue", sender: self)
        default: break
        }
    }
    
    // this will handle the use wanting to logout
    
    @IBAction func gearButtonWasPressed(sender: UIButton) {
        
        // Create an option menu as an action sheet
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        optionMenu.addAction(cancelAction)
        
        // create a closure for when the user taps the logout button.
        let logoutActionHandler = { (action:UIAlertAction!) -> Void in
            //FIRAuth.signOut(FIRAuth.auth()?.currentUser)
            do {
                try FIRAuth.auth()?.signOut()
            } catch {
                print(error)
            }
            
            self.performSegueWithIdentifier("userHasLoggedOutSegue", sender: self)
        }
        
        let callAction = UIAlertAction(title: "Logout?", style: UIAlertActionStyle.Default, handler: logoutActionHandler)
        
        optionMenu.addAction(callAction)
        
        // Display the menu
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    
    
    // pass the childbyautoid keys and the snapshot
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "allNotesToEditPlainNoteSegue" {
            if let destVc = segue.destinationViewController as? EditPlainNoteViewController {
                destVc.specificRef = self.specificRef!
                destVc.specificSnapshot = self.specificSnapshot!
            }
        }
        
        if segue.identifier == "allNotesToEditTask2Segue" {
            if let destVc = segue.destinationViewController as? EditTask2ViewController {
                destVc.specificRef = self.specificRef!
                destVc.specificSnapshot = self.specificSnapshot!
            }
        }
    }
}














