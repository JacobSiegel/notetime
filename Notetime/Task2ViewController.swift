// Notes:
// Everything i do here requires at least two variables... one for each view.
// the only way it works is if they are passed back and forth throughout the app.
// time may be way slowed down.. but we'll see.
// I don't see a better way to do this.. plus, it is 1000000X more productive than saving things to firebase!!

import UIKit
import FirebaseAuth
import FirebaseDatabase

// This class will now represent 2 views... the table view and the config.
class Task2ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // references to the database
    let usersRef = FIRDatabase.database().reference().child("users")
    let usersId = FIRAuth.auth()?.currentUser?.uid
    
    // variables that take care of the the number of cells between the two view controllers.
    var numberOfCellsInTableView = 1
    var numberOfCellsInConfig = 1
    
    // a reference to the tableView so that i can reload it.
    @IBOutlet weak var tableView: UITableView!
    
    // title of the tableview scene
    @IBOutlet weak var titleOutlet: UITextField!
    var tableViewTitle = ""
    var configTitle = ""
    
    // create arrays to store the notes locally and temporarily. 
    var taskDescriptionsInTableView: [String] = ["Tap to edit"]
    var timesInTableView: [String] = ["Tap to edit"]
    var timesInTableViewAsDate: [NSDate?] = [nil]
    
    var taskDescriptionsInConfig = [String]()
    var timesInConfig = [String]()
    var timesInConfigAsDate = [NSDate?]()
    
    // get outlets the the time picker and the text view
    @IBOutlet weak var configTextView: UITextView!
    var currentTaskDisplayedForTableView = ""
    var currentTaskDisplayedForConfig = ""
    
    // took out the current time because i am using a date object array instead.
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var currentTimeForTableviewAsDate: NSDate?
    var currentTimeForConfigAsDate: NSDate?
    
    
    // grabbing outlets to format the tableview
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var addTaskButton: UIButton!
    
    // grabbing outlets to format the config view
    @IBOutlet weak var saveButtonInConfig: UIButton!
    @IBOutlet weak var taskDescriptionView: UIView!
    
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var timeBackGround: UIView!
    
    var valueOfIsEditedInTableView = false
    var valueForIsEditedInConfig = false
    
    var isEditedInTableView: [Bool] = [false]
    var isEditedInConfig: [Bool] = [false]
    
    var indexPathRow: Int?
    
    var uniqueIDsForNotifications = [String]()
    
    
    // used for displaying error messages
    func displayVC(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func setViewForTableView() {
        self.saveButton.layer.cornerRadius = 15
        self.addTaskButton.layer.cornerRadius = 15
    }
    
    
    func setViewForConfigView() {
        self.saveButtonInConfig.layer.cornerRadius = 15
        self.taskDescriptionView.layer.cornerRadius = 15
        self.timeView.layer.cornerRadius = 15
        self.timeBackGround.layer.cornerRadius = 15
        self.configTextView.layer.cornerRadius = 15
        
        func prefersStatusBarHidden() -> Bool {
            return true
        }
    }
    
    
    // set up notifications
    func setUpNotifications() {
        
        // get the current notification settings
        let notificationSettings: UIUserNotificationSettings! = UIApplication.sharedApplication().currentUserNotificationSettings()
        
        // check and see if there are no permissions
        if (notificationSettings.types == UIUserNotificationType.None) {
            
            // ask for permission
            let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Sound, UIUserNotificationType.Badge]
            
            // set up the action
            let justInformAction = UIMutableUserNotificationAction()
            justInformAction.identifier = "justInform"
            justInformAction.title = "OK, got it"
            justInformAction.activationMode = UIUserNotificationActivationMode.Background
            justInformAction.destructive = false
            justInformAction.authenticationRequired = false
            
            let actionsArray: [UIUserNotificationAction] = [justInformAction]
            
            // Specify the category related to the above action
            let timedTaskReminderCategory = UIMutableUserNotificationCategory()
            timedTaskReminderCategory.identifier = "timeToCompleteTaskCategory"
            timedTaskReminderCategory.setActions(actionsArray, forContext: UIUserNotificationActionContext.Default)
            
            let categoriesForSettings = NSSet(objects: timedTaskReminderCategory)
            
            let newNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: (categoriesForSettings as! Set<UIUserNotificationCategory>))
            UIApplication.sharedApplication().registerForRemoteNotifications()
            UIApplication.sharedApplication().registerUserNotificationSettings(newNotificationSettings)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpNotifications()
        
        let restorationId = self.restorationIdentifier!
        
        if restorationId == "TableView" {
            self.setViewForTableView()
        }
        if restorationId == "config" {
            self.setViewForConfigView()
            hideKeyboardWhenTappedAround()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        
        // this works with the passing of the data between the two views.
        if tableViewTitle != "" {
            self.titleOutlet.text! = self.tableViewTitle
        }
        
        // Check to see what view is currently displayed.
        let restorationId = self.restorationIdentifier!
        
        if restorationId == "TableView" {
            self.tableView.reloadData()
        }
        
        if restorationId == "config" {
            if self.currentTaskDisplayedForConfig != "Tap to edit" {
                self.configTextView.text = self.currentTaskDisplayedForConfig
                
                if self.configTextView.text != "" {
                    // this will set the uidatepicker to the date that the user has chosen.
                    let date = self.currentTimeForConfigAsDate!
                    datePicker.setDate(date, animated: true)
                }
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    // MARK: - Setup the tableview
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfCellsInTableView
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! Task2TableViewCell
        
        // configuring cell
        cell.taskDescription.text = self.taskDescriptionsInTableView[indexPath.row]
        cell.time.text = self.timesInTableView[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == .Delete {
            
            if self.numberOfCellsInTableView == 1 {
                displayVC("You cannot delete the last task")
            } else {
                self.taskDescriptionsInTableView.removeAtIndex(indexPath.row)
                self.timesInTableView.removeAtIndex(indexPath.row)
                self.timesInTableViewAsDate.removeAtIndex(indexPath.row)
                self.isEditedInTableView.removeAtIndex(indexPath.row)
                self.numberOfCellsInTableView -= 1
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Add Cells
    @IBAction func addTaskButtonWasPressed(sender: UIButton) {
        
        if self.taskDescriptionsInTableView.last! == "Tap to edit" {
            self.displayVC("Make sure you fill out the provided cell first")
        } else {
        
            numberOfCellsInTableView += 1
        
            // add a blank string and nil to the arrays
            self.taskDescriptionsInTableView.append("Tap to edit")
            self.timesInTableView.append("Tap to edit")
            self.timesInTableViewAsDate.append(nil)
            self.isEditedInTableView.append(false)
        
            // first time in this VC that I am reloading the tableview.
            self.tableView.reloadData()
        }
    }
    
    
    // MARK: - Navigation
    @IBAction func navButtonWasPressedForAllTableView(sender: UIButton) {
        
        let button = sender.currentTitle!
        
        switch button {
        case "📖 All Notes":
            self.performSegueWithIdentifier("timedTaskToAllNotesSegue", sender: self)
        case "📝 Plain Note":
            self.performSegueWithIdentifier("timedTaskToPlainNoteSegue", sender: self)
        case "Save":
            self.performSegueWithIdentifier("timedTaskToAllNotesSegue", sender: self)
        default:
            break
        }
        
        self.scheduleLocalNotifications()
    }
    
    @IBAction func navButtonWasPressedForConfig(sender: UIButton) {
        self.performSegueWithIdentifier("backFromConfigSegue", sender: self)
    }
    
    func passDataFromTableViewToConfig(destVc: Task2ViewController) {
        // pass number of cells
        destVc.numberOfCellsInConfig = self.numberOfCellsInTableView
        
        //pass the title
        destVc.configTitle = self.titleOutlet.text!
        
        // pass the content of the array
        destVc.taskDescriptionsInConfig = self.taskDescriptionsInTableView
        destVc.timesInConfig = self.timesInTableView
        destVc.timesInConfigAsDate = self.timesInTableViewAsDate
        destVc.isEditedInConfig = self.isEditedInTableView
        
        let row = self.tableView.indexPathForSelectedRow!.row
        destVc.indexPathRow = row
        
        
        self.currentTaskDisplayedForTableView = self.taskDescriptionsInTableView[row]
        self.currentTimeForTableviewAsDate = self.timesInTableViewAsDate[row]
        self.valueOfIsEditedInTableView = self.isEditedInTableView[row]
        
        if self.currentTaskDisplayedForTableView != "Tap to edit" {
            
            // pass time and task and date here
            destVc.currentTaskDisplayedForConfig = self.currentTaskDisplayedForTableView
            destVc.currentTimeForConfigAsDate = self.currentTimeForTableviewAsDate
            
            // so now i have the value of the true or false.
            destVc.valueForIsEditedInConfig = self.valueOfIsEditedInTableView
        }
        
    }
    
    func passDataFromConfigToTableView(destVc: Task2ViewController) {
        
        // pass number of cells
        destVc.numberOfCellsInTableView = self.numberOfCellsInConfig
        
        // pass the title
        destVc.tableViewTitle = self.configTitle
        
        // append the date and time to the config arrays if the task description is not blank
        if configTextView.text != "" {
            let task = configTextView.text
            let timeAsString = NSDateFormatter.localizedStringFromDate(self.datePicker.date, dateStyle: NSDateFormatterStyle.ShortStyle, timeStyle: .ShortStyle)
            let timeAsDate = self.datePicker.date
            
            
            // here we take out the old value and insert the new one
            if self.valueForIsEditedInConfig == true {
                
                self.taskDescriptionsInConfig.removeAtIndex(self.indexPathRow!)
                self.taskDescriptionsInConfig.insert(task, atIndex: self.indexPathRow!)
                
                self.timesInConfig.removeAtIndex(self.indexPathRow!)
                self.timesInConfig.insert(timeAsString, atIndex: self.indexPathRow!)
                
                self.timesInConfigAsDate.removeAtIndex(self.indexPathRow!)
                self.timesInConfigAsDate.insert(timeAsDate, atIndex: self.indexPathRow!)
                
            } else {
                
                self.taskDescriptionsInConfig.removeLast()
                self.taskDescriptionsInConfig.append(task)
                
                self.timesInConfig.removeLast()
                self.timesInConfig.append(timeAsString)
                
                self.timesInConfigAsDate.removeLast()
                self.timesInConfigAsDate.append(timeAsDate)
                
                self.isEditedInConfig.removeLast()
                self.isEditedInConfig.append(true)
            }
        }
        
        // set the arrays equal to each other.
        destVc.taskDescriptionsInTableView = self.taskDescriptionsInConfig
        destVc.timesInTableView = self.timesInConfig
        destVc.timesInTableViewAsDate = self.timesInConfigAsDate
        destVc.isEditedInTableView = self.isEditedInConfig
    }
    
    // MARK: - Passing data between the view controllers.
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "configSegue" {
            let destVc = segue.destinationViewController as! Task2ViewController
            self.passDataFromTableViewToConfig(destVc)
        }
        
        if segue.identifier == "backFromConfigSegue" {
            let destVc = segue.destinationViewController as! Task2ViewController
            self.passDataFromConfigToTableView(destVc)
        }
    }
    
    // MARK: - Pushing the data stored in the arrays to firebase.
    func grabDataInArraysAndPushToFirebase() {
        
        let title = self.titleOutlet.text!
        
        if title != "" && (self.taskDescriptionsInTableView[0] == "Tap to edit" && self.timesInTableView[0] == "Tap to edit") {
            
            print("Dont execute following")
        }
        
        else if title != "" || (self.taskDescriptionsInTableView[0] != "Tap to edit" && self.timesInTableView[0] != "Tap to edit") {
            
            let data = [
                "title": "🕔 " + title,
                "noteType": "TimedTask",
                "Tasks": self.taskDescriptionsInTableView,
                "Times": self.timesInTableView,
                "IsEditedValues": self.isEditedInTableView,
                "UniqueIDsForNotifications": self.uniqueIDsForNotifications
            ]
            self.usersRef.child(usersId!).childByAutoId().setValue(data)
        }
    }
    
    // NOTE: - if 2 notifications are created with the same time only one of them will be created.
    
    // so what did i do here....
    /*
     1.) check if there are dates that need to be turned into notifications.
     2.) go through the dates array and create a notification for each date
     3.) give each notification a unique identifier
     4.) save the unique id in a property array as well inside of this view controle\ler
     5.) Finally schedule the application.
     */
    func scheduleLocalNotifications() {
        
        if timesInTableViewAsDate.count > 0 {
            
            for i in 0...timesInTableViewAsDate.count - 1 {
                
                // need to check if more than 1 times are the same:
                if self.taskDescriptionsInTableView[i] != "Tap to edit" {
                    
                    if self.timesInTableViewAsDate[i]!.isEqualToDate(NSDate()) {
                        print("Times are equal")
                    }
                    
                    // implement check to see if the current date is greater than the date that the user entered
                    
                    if timesInTableViewAsDate[i]!.timeIntervalSinceNow.isSignMinus {
                        print("Date that user has entered is equal or before than Now")
                        self.uniqueIDsForNotifications.append("No notification sceduled")
                    } else {
                        let localNotification = UILocalNotification()
                        localNotification.fireDate = timesInTableViewAsDate[i]!
                        localNotification.alertBody = "Hey, it's time to complete \(self.taskDescriptionsInTableView[i])"
                        localNotification.soundName = "alarm.wav"
                        localNotification.alertAction = "All Notes"
                        localNotification.category = "timeToCompleteTaskCategory"
                        localNotification.applicationIconBadgeNumber = 1
                        
                        // generate uid.
                        let uid = NSUUID().UUIDString
                        
                        // create a unique id dictionary and add it to the localNotification
                        let infoDict = ["uid" : uid]
                        localNotification.userInfo = infoDict
                        
                        // append to the array here.
                        self.uniqueIDsForNotifications.append(uid)
                        
                        // finally, schedule the actual notification.
                        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                    }
                }
            }
        }
        
        self.grabDataInArraysAndPushToFirebase()
    }
}

/*
 
NOTES:
 
 
Following hypotheses was not correct... need to get a unique id and then store times in an array:
 
need to check if the time is already there... although maybe wont have to.
goal: go through the array of notifications. if the sceduled notification is equal to an element in the full loop of the 
notifications.. then i can find its index in that array and then i can push it up to firebase.

will need to implement checks here... but this is still very new

I should add the elements to an array and then pass it over to the edit view controller... go through a loop and then delete any notifications with the index that we stored here... and then scedule new ones. i think this should work.
 
1.) Passing the number of cells between each view works without firebase hack! I'm passing data between the view controllers
    using the prepareForSegue Method.  This may be a bit of a hack as well... but it works for now and i'm not messing with
    it.
 
2.) Figure out how to save views locally without pushing to firebase. Firebase push will be at the end.
 
3.) Figure out how to pass the text from the one vc to the other.. what i have right now is not working.
 
4.) Figure out how to format a date and then display it so that users can pickup where they left off.... have to do some debugging!!!!!!!
 
5.) Bug when going back to an original cell after adding a cell. The text goes to both cells. Easiest way to fix this would just be to destroy the extra cell with tap to edit.
 
6.) Add everything to the edit view controller and figure out push notifications and search.

*/















