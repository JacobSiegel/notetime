import UIKit
import FirebaseAuth
import FirebaseDatabase

class EditTask2ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // Firebase references
    let usersRef = FIRDatabase.database().reference().child("users")
    let usersId = FIRAuth.auth()?.currentUser?.uid
    
    // variables that take care of the the number of cells between the two view controllers.
    var numberOfCellsInTableView: Int?
    var numberOfCellsInConfig: Int?
    
    // create arrays to store the notes locally and temporarily.
    var taskDescriptionsInTableView = [String]()
    var timesInTableView = [String]()
    
    var taskDescriptionsInConfig = [String]()
    var timesInConfig = [String]()
    
    // childbyautoid key and specific snapshot that is passed from all notes vc
    var specificRef: String?
    var specificRefInConfig: String?
    
    var specificSnapshot: FIRDataSnapshot?
    
    // outlet to title
    @IBOutlet weak var titleTextField: UITextField!
    var tableViewTitle = ""
    var configTitle = ""

    // outlet to tableview
    @IBOutlet weak var tableView: UITableView!
    
    // textview for the config view
    @IBOutlet weak var configTextView: UITextView!
    var currentTaskDisplayedForTableView = ""
    var currentTaskDisplayedForConfig = ""
    
    // reference to the datepicker
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var currentTimeInTableView = ""
    var currentTimeInConfig = ""
    
    // get properties to edit the tableView
    @IBOutlet weak var saveButtonInTableView: UIButton!
    @IBOutlet weak var addTaskButtonInTableView: UIButton!
    
    // get properties to edit the config view
    @IBOutlet weak var saveButtonInConfigView: UIButton!
    @IBOutlet weak var taskView: UIView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var timeBackGround: UIView!

    // its bug squashing time!!!!
    var valueOfIsEditedInTableView = false
    var valueForIsEditedInConfig = false
    
    var isEditedInTableView = [Bool]()
    var isEditedInConfig = [Bool]()
    
    var indexPathRow: Int?
    
    var uniqueIDsForNotifications = [String]()
    var uniqueIDSsInConfig = [String]()
    
//    var firstTimeInTableView = true
//    var firstTimeInTableForConfig: Bool?
    
    
    // used for displaying error messages
    func displayVC(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func setEditTableViewProperties() {
        self.saveButtonInTableView.layer.cornerRadius = 15
        self.addTaskButtonInTableView.layer.cornerRadius = 15
    }
    
    func setEditConfigProperties() {
        self.saveButtonInConfigView.layer.cornerRadius = 15
        self.taskView.layer.cornerRadius = 15
        self.timeView.layer.cornerRadius = 15
        self.timeBackGround.layer.cornerRadius = 15
        self.configTextView.layer.cornerRadius = 15
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    // MARK: - Setting up view and getting data from firebase to display.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let restorationId = self.restorationIdentifier!
        
        if restorationId == "EditTableView" {
            
            // print("number scheduled: \(UIApplication.sharedApplication().scheduledLocalNotifications!.count)")
            
//            if self.firstTimeInTableView {
//                
//                print("This executed...")
//                
//                self.deleteNotificationsAndUniqueIDs()
//                
//                self.firstTimeInTableView = false
//                self.firstTimeInTableForConfig = false
//            }
            
            self.setEditTableViewProperties()
        }
        
        if restorationId == "EditConfig" {
            self.setEditConfigProperties()
            hideKeyboardWhenTappedAround()
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        if let snapShot = specificSnapshot {
            
            var title = snapShot.value?.objectForKey("title") as! String
            let tasks = snapShot.value?.objectForKey("Tasks") as! NSArray as! [String]
            let times = snapShot.value?.objectForKey("Times") as! NSArray as! [String]
            
            let isEditedValues = snapShot.value?.objectForKey("IsEditedValues") as! NSArray as! [Bool]
            let localUniqueIDsForNotifications = snapShot.value?.objectForKey("UniqueIDsForNotifications") as! NSArray as! [String]
            
            
            title = String(title.characters.dropFirst())
            title = String(title.characters.dropFirst())
            self.titleTextField.text = title
            
            // handle populating arrays
            self.taskDescriptionsInTableView = tasks
            self.timesInTableView = times
            self.isEditedInTableView = isEditedValues
            self.uniqueIDsForNotifications = localUniqueIDsForNotifications
            // self.uniqueIDSsInConfig = localUniqueIDsForNotifications
            
            // take care of the number of cells
            self.numberOfCellsInTableView = tasks.count
            self.numberOfCellsInConfig = tasks.count
        }
        
        // this works with the passing of the data between the two views.
        if tableViewTitle != "" {
            self.titleTextField.text! = self.tableViewTitle
        }

        
        // Check to see what view is currently displayed.
        let restorationId = self.restorationIdentifier!
        
        if restorationId == "EditTableView" {
            self.tableView.reloadData()
        }
        
        if restorationId == "EditConfig" {
            
            if self.currentTaskDisplayedForConfig != "Tap to edit" {
                
                // set the config text view with the task that was clicked on.
                self.configTextView.text = self.currentTaskDisplayedForConfig
                
                // format and set the date.
                if self.configTextView.text != "" {
                    let dateFromatter = NSDateFormatter()
                    dateFromatter.dateFormat = "M/d/yy, h:mm a"
                
                    if let date = dateFromatter.dateFromString(self.currentTimeInConfig) {
                        self.datePicker.setDate(date, animated: true)
                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addTaskButtonWasPressed(sender: UIButton) {
        if self.taskDescriptionsInTableView.last! == "Tap to edit" {
            self.displayVC("Make sure you fill out the provided cell first")
        } else {
            self.numberOfCellsInTableView!+=1
            
            // add a blank string to the arrays
            self.taskDescriptionsInTableView.append("Tap to edit")
            self.timesInTableView.append("Tap to edit")
            self.isEditedInTableView.append(false)
            
            self.tableView.reloadData()
        }

    }
    
    
    // MARK: - Setup the tableview
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if numberOfCellsInTableView != nil {
            return numberOfCellsInTableView!
        } else {
            return taskDescriptionsInTableView.count
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! EditTask2TableViewCell
        
        cell.taskDescription.text = taskDescriptionsInTableView[indexPath.row]
        cell.time.text = timesInTableView[indexPath.row]
        return cell
    }
    
    
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == .Delete {
            
            if self.numberOfCellsInTableView == 1 {
                displayVC("You cannot delete the last task")
            } else {
                self.taskDescriptionsInTableView.removeAtIndex(indexPath.row)
                self.timesInTableView.removeAtIndex(indexPath.row)
                self.isEditedInTableView.removeAtIndex(indexPath.row)
                self.numberOfCellsInTableView! -= 1
                
                // this should work for deleting from the tableview.
                if self.uniqueIDsForNotifications.count != 0 {
                    self.uniqueIDsForNotifications.removeAtIndex(indexPath.row)
                }
                
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - Navigation    
    @IBAction func navButtonWasPressedFromTableView(sender: UIButton) {
        
        self.scheduleLocalNotifications()
        
        self.performSegueWithIdentifier("backToAllNotesSegueFromEditTimedTask", sender: self)
        
    }
    
    @IBAction func navButtonWasPressedFromConfig(sender: UIButton) {
        self.performSegueWithIdentifier("backToEditTimedTaskSegue", sender: self)
    }
    
    // MARK: - Passing data between views
    func passDataFromTableViewToConfig(destVc: EditTask2ViewController) {
        
        // pass the unique ids
        destVc.uniqueIDSsInConfig = self.uniqueIDsForNotifications
        
        // pass the bool for if it is the users first time in config
        // destVc.firstTimeInTableForConfig = self.firstTimeInTableView
        
        // pass number of cells
        destVc.numberOfCellsInConfig = self.numberOfCellsInTableView!
        
        //pass the title
        destVc.configTitle = self.titleTextField.text!
        
        // pass the specific reference
        destVc.specificRefInConfig = self.specificRef!
        
        // pass the content of the array
        destVc.taskDescriptionsInConfig = self.taskDescriptionsInTableView
        destVc.timesInConfig = self.timesInTableView
        destVc.isEditedInConfig = self.isEditedInTableView
        
        // new code for setting the text view in the config view.
        let row = self.tableView.indexPathForSelectedRow!.row
        destVc.indexPathRow = row
        
        self.currentTaskDisplayedForTableView = self.taskDescriptionsInTableView[row]
        
        // get current time in table view
        self.currentTimeInTableView = self.timesInTableView[row]
        
        // get the true or false value
        self.valueOfIsEditedInTableView = self.isEditedInTableView[row]
        
        if self.currentTaskDisplayedForTableView != "Tap to edit" {
            destVc.currentTaskDisplayedForConfig = self.currentTaskDisplayedForTableView
            destVc.currentTimeInConfig = self.currentTimeInTableView
            
            // so now i have the value of the true or false.
            destVc.valueForIsEditedInConfig = self.valueOfIsEditedInTableView
        }
    }
    
    func passDataFromConfigToTableView(destVc: EditTask2ViewController) {
        
        // pass back the number of ids in config
        destVc.uniqueIDsForNotifications = self.uniqueIDSsInConfig
        
        // pass the bool for if it is the users first time in config
        //destVc.firstTimeInTableView = self.firstTimeInTableForConfig!
        
        // pass number of cells
        destVc.numberOfCellsInTableView = self.numberOfCellsInConfig!
        
        // pass the title
        destVc.tableViewTitle = self.configTitle
        
        // pass the reference 
        destVc.specificRef = self.specificRefInConfig!
        
        
        // append the date and time to the config arrays if the task description is not blank
        if configTextView.text != "" {
            let task = configTextView.text
            let timeAsString = NSDateFormatter.localizedStringFromDate(self.datePicker.date, dateStyle: .ShortStyle, timeStyle: .ShortStyle)
            
            if self.valueForIsEditedInConfig == true {
                
                self.taskDescriptionsInConfig.removeAtIndex(self.indexPathRow!)
                self.taskDescriptionsInConfig.insert(task, atIndex: self.indexPathRow!)
                
                self.timesInConfig.removeAtIndex(self.indexPathRow!)
                self.timesInConfig.insert(timeAsString, atIndex: self.indexPathRow!)
                
            } else {
                
                self.taskDescriptionsInConfig.removeLast()
                self.taskDescriptionsInConfig.append(task)
                
                self.timesInConfig.removeLast()
                self.timesInConfig.append(timeAsString)
                
                self.isEditedInConfig.removeLast()
                self.isEditedInConfig.append(true)
            }
        }
        
        // set the arrays equal to each other.
        destVc.taskDescriptionsInTableView = self.taskDescriptionsInConfig
        destVc.timesInTableView = self.timesInConfig
        destVc.isEditedInTableView = self.isEditedInConfig
    }


    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
       
        if segue.identifier == "goingToConfigSegue" {
            let destVc = segue.destinationViewController as! EditTask2ViewController
            self.passDataFromTableViewToConfig(destVc)
        }
        
        if segue.identifier == "backToEditTimedTaskSegue" {
            let destVc = segue.destinationViewController as! EditTask2ViewController
            self.passDataFromConfigToTableView(destVc)
        }
    }
    
    // MARK: - Pushing the data stored in the arrays to firebase.
    func grabDataInArraysAndPushToFirebase() {
        
        let title = self.titleTextField.text!
        
        if title != "" || self.taskDescriptionsInTableView.last! != "Tap to edit" {
            
            let data: [NSObject: AnyObject] = [
                "title": "🕔 " + title,
                "noteType": "TimedTask",
                "Tasks": self.taskDescriptionsInTableView,
                "Times": self.timesInTableView,
                "IsEditedValues": self.isEditedInTableView,
                "UniqueIDsForNotifications": self.uniqueIDsForNotifications
            ]
            
            self.usersRef.child(usersId!).child(self.specificRef!).updateChildValues(data)
        }
    }
    
    func formatDate(stringDate: String) -> NSDate {
        
        let dateFromatter = NSDateFormatter()
        dateFromatter.dateFormat = "M/d/yy, h:mm a"
        return dateFromatter.dateFromString(stringDate)!
    }
    
    func deleteNotificationsAndUniqueIDs() {
        
        let uniqueIDNotificationCount = self.uniqueIDsForNotifications.count
        
        var notificationsToDelete = [UILocalNotification]()
        
        print("unique id count is: \(self.uniqueIDsForNotifications.count)")
        // go through unique id array first
        for i in 0..<uniqueIDNotificationCount {
            
            // grad the unique id that will be compared to every other notifications id
            let storedID = self.uniqueIDsForNotifications[i]
            
            // now go through sceduled notifications array
            
            let scheduledNotificationCount = UIApplication.sharedApplication().scheduledLocalNotifications!.count
            print("Count of all notifications is: \(scheduledNotificationCount)")
            
            if scheduledNotificationCount != 0 {
            
                for x in 0..<scheduledNotificationCount {
                
                    let notification = UIApplication.sharedApplication().scheduledLocalNotifications![x]
                
                    let notificationUid = String(notification.userInfo!["uid"]!)

                
                    if storedID == notificationUid {
                        
                        notificationsToDelete.append(notification)
                        
                    }
                }
            }
        }
        
        for x in notificationsToDelete {
            UIApplication.sharedApplication().cancelLocalNotification(x)
        }
        
        self.uniqueIDsForNotifications.removeAll()
    }
    
    
    func scheduleLocalNotifications() {
            
        self.deleteNotificationsAndUniqueIDs()
            
        print("times in tableview is: \(timesInTableView.count - 1)")
        
        for i in 0..<timesInTableView.count {
            print("Index is: \(i)")
            
            if self.taskDescriptionsInTableView[i] != "Tap to edit" {
                    
                // create local notification
                    
                if self.formatDate(timesInTableView[i]).timeIntervalSinceNow.isSignMinus {
                    print("Wont work becuase the users time is less that the current time")
                    self.uniqueIDsForNotifications.append("No notification was scheduled")
                } else {
                    let localNotification = UILocalNotification()
                    localNotification.fireDate = self.formatDate(timesInTableView[i])
                    localNotification.alertBody = "Hey, it's time to complete \(self.taskDescriptionsInTableView[i])"
                    localNotification.soundName = "alarm.wav"
                    localNotification.alertAction = "All Notes"
                    localNotification.category = "timeToCompleteTaskCategory"
                    localNotification.applicationIconBadgeNumber = 1
                        
                    // generate uid.
                    let uid = NSUUID().UUIDString
                        
                    // create a unique id dictionary and add it to the localNotification
                    let infoDict = ["uid" : uid]
                    localNotification.userInfo = infoDict
                        
                    // append to the array here.
                    self.uniqueIDsForNotifications.append(uid)
                        
                    // finally, schedule the actual notification.
                    UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                }
            }
        }
            
        self.grabDataInArraysAndPushToFirebase()
    }
}









