import UIKit
import FirebaseAuth
import FirebaseDatabase

class EditPlainNoteViewController: UIViewController {

    @IBOutlet weak var plainNoteTitleTextField: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var plainNoteBodyTextView: UITextView!
    var specificRef: String?
    var specificSnapshot: FIRDataSnapshot?
    let usersRef = FIRDatabase.database().reference().child("users")
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.doneButton.layer.cornerRadius = 15
        self.plainNoteBodyTextView.layer.cornerRadius = 15
    }
    
    override func viewDidAppear(animated: Bool) {
        if let snapShot = specificSnapshot {
            var title = snapShot.value?.objectForKey("title") as! String
            let noteBody = snapShot.value?.objectForKey("note") as! String
            
            title = String(title.characters.dropFirst())
            title = String(title.characters.dropFirst())
            
            plainNoteTitleTextField.text = title
            plainNoteBodyTextView.text = noteBody
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // update the firebase data reference that was passed to us from the all note view controller
    func grabAndUpdateFirebase() {
        let title = plainNoteTitleTextField.text!
        let note = plainNoteBodyTextView.text!
        let usersId = FIRAuth.auth()?.currentUser?.uid
        
        if title != "" || note != "" {
            let updatedData = [
                "title": "📝 " + title,
                "note": note,
                "noteType": "PlainNote"
            ]
            self.usersRef.child(usersId!).child(self.specificRef!).updateChildValues(updatedData)
        }
    }
    
    // MARK:- Navigation
    @IBAction func navButtonWasPressed(sender: UIButton) {
        grabAndUpdateFirebase()
        self.performSegueWithIdentifier("backToAllNotesFromEditPlainNote", sender: self)
    }
}
